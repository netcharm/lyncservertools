﻿namespace LyncEdgeDynaDNSUpdater
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.AppTimer = new System.Windows.Forms.Timer(this.components);
            this.AppTray = new System.Windows.Forms.NotifyIcon(this.components);
            this.edLog = new System.Windows.Forms.TextBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbDynaDNSFQDN = new System.Windows.Forms.Label();
            this.edEdge = new System.Windows.Forms.TextBox();
            this.edDynaDNS = new System.Windows.Forms.TextBox();
            this.edTimerInterval = new System.Windows.Forms.NumericUpDown();
            this.cbAutoUpdate = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.edTimerInterval)).BeginInit();
            this.SuspendLayout();
            // 
            // AppTimer
            // 
            this.AppTimer.Enabled = true;
            this.AppTimer.Interval = 5000;
            this.AppTimer.Tick += new System.EventHandler(this.AppTimer_Tick);
            // 
            // AppTray
            // 
            this.AppTray.Text = "Update DynaDNS Info to Lync Server 2010 Edge Server";
            this.AppTray.Visible = true;
            // 
            // edLog
            // 
            this.edLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edLog.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edLog.Location = new System.Drawing.Point(14, 116);
            this.edLog.Multiline = true;
            this.edLog.Name = "edLog";
            this.edLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.edLog.Size = new System.Drawing.Size(438, 213);
            this.edLog.TabIndex = 1;
            this.edLog.WordWrap = false;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnUpdate.Location = new System.Drawing.Point(377, 12);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 88);
            this.btnUpdate.TabIndex = 2;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(14, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "Lync Edge Server FQDN:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbDynaDNSFQDN
            // 
            this.lbDynaDNSFQDN.Location = new System.Drawing.Point(14, 9);
            this.lbDynaDNSFQDN.Name = "lbDynaDNSFQDN";
            this.lbDynaDNSFQDN.Size = new System.Drawing.Size(180, 20);
            this.lbDynaDNSFQDN.TabIndex = 6;
            this.lbDynaDNSFQDN.Text = "DynaDNS FQDN:";
            this.lbDynaDNSFQDN.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // edEdge
            // 
            this.edEdge.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edEdge.Location = new System.Drawing.Point(14, 79);
            this.edEdge.Name = "edEdge";
            this.edEdge.Size = new System.Drawing.Size(263, 21);
            this.edEdge.TabIndex = 4;
            // 
            // edDynaDNS
            // 
            this.edDynaDNS.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edDynaDNS.Location = new System.Drawing.Point(14, 32);
            this.edDynaDNS.Name = "edDynaDNS";
            this.edDynaDNS.Size = new System.Drawing.Size(263, 21);
            this.edDynaDNS.TabIndex = 3;
            // 
            // edTimerInterval
            // 
            this.edTimerInterval.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.edTimerInterval.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.edTimerInterval.Location = new System.Drawing.Point(293, 32);
            this.edTimerInterval.Maximum = new decimal(new int[] {
            240,
            0,
            0,
            0});
            this.edTimerInterval.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.edTimerInterval.Name = "edTimerInterval";
            this.edTimerInterval.Size = new System.Drawing.Size(69, 21);
            this.edTimerInterval.TabIndex = 0;
            this.edTimerInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.edTimerInterval.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // cbAutoUpdate
            // 
            this.cbAutoUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAutoUpdate.AutoEllipsis = true;
            this.cbAutoUpdate.Checked = true;
            this.cbAutoUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAutoUpdate.Location = new System.Drawing.Point(293, 81);
            this.cbAutoUpdate.Name = "cbAutoUpdate";
            this.cbAutoUpdate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbAutoUpdate.Size = new System.Drawing.Size(69, 16);
            this.cbAutoUpdate.TabIndex = 7;
            this.cbAutoUpdate.Text = "Auto";
            this.cbAutoUpdate.UseVisualStyleBackColor = true;
            this.cbAutoUpdate.CheckedChanged += new System.EventHandler(this.cbAutoUpdate_CheckedChanged);
            // 
            // MainForm
            // 
            this.AcceptButton = this.btnUpdate;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 342);
            this.Controls.Add(this.cbAutoUpdate);
            this.Controls.Add(this.lbDynaDNSFQDN);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.edEdge);
            this.Controls.Add(this.edDynaDNS);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.edLog);
            this.Controls.Add(this.edTimerInterval);
            this.MinimumSize = new System.Drawing.Size(320, 240);
            this.Name = "MainForm";
            this.Text = "Lync Edge Server DynaDNS Update Topology Tool";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.edTimerInterval)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer AppTimer;
        private System.Windows.Forms.NotifyIcon AppTray;
        private System.Windows.Forms.NumericUpDown edTimerInterval;
        private System.Windows.Forms.TextBox edLog;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.TextBox edDynaDNS;
        private System.Windows.Forms.TextBox edEdge;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbDynaDNSFQDN;
        private System.Windows.Forms.CheckBox cbAutoUpdate;
    }
}

