###############################################################################
#
# Automatic publishing DynaDNS doamainname to Lync Topology edge pool's
# External IP binding.
# Author: netcharm@163.com, 2012-03-17
#
###############################################################################

Import-Module Lync

#[System.Xml.XmlDocument]

function ChangeXML
{
    $filename = $args[0]
    $ip = $args[1]
    $servername = $args[2]

    $cfgXml = New-Object Xml.XmlDocument
    $cfgXml.Load($filename)

    $cfgRoot = $cfgXml.Get_DocumentElement()

    $machinelist = $cfgRoot.GetElementsByTagName("Machine");
    foreach( $machine in $machinelist)
    {
        $machineattrs = $machine.Attributes
        $machinename = $machineattrs.GetNamedItem("Fqdn").Get_Value()
        if($machinename -ieq $servername)
        {
            $interfacelist = $machine.GetElementsByTagName("NetInterface");
            foreach($interface in $interfacelist)
            {
                $attrs = $interface.Attributes
                $interfaceSide = $attrs.GetNamedItem("InterfaceSide").Get_Value()
                if( $interfaceSide -ieq "External")
                {
                    try
                    {
                        $ConfiguredIPAddress = $attrs.GetNamedItem("ConfiguredIPAddress").Get_Value()
                        if( $ConfiguredIPAddress -is [String])
                        {
                            echo ("Old IP Addreee : " + $dynahostname +" -> "+ $ConfiguredIPAddress)
                            echo ("New IP Address : " + $dynahostname +" -> "+ $ip)
                            $attrs.GetNamedItem("ConfiguredIPAddress").Set_Value($ip)
                            echo ("External dynamic IP address " + $ip + " wrote to Topology XML config file.")
                        }
                    }
                    catch{}
                }
            }
        }
    }
    $cfgXml.Save($filename)
}

try
{
    $dynahostname = $args[0]
    $servername = $args[1]
    $dynaip = [System.Net.Dns]::GetHostByName($dynahostname).AddressList[0].ToString()
    $cfgfilename = ".\" + $args[2]

    echo ("DynaDNS : " + $dynahostname)
    echo ("Edge    : " + $servername)
    echo ("Dyna IP : " + $dynaip)
    echo ("CfgName : " + $cfgfilename)

    $simpleUrlObj = Get-CsSimpleUrlConfiguration

    (Get-CsTopology -AsXml).ToString() > $cfgfilename

    ChangeXML $cfgfilename $dynaip $servername

    Publish-CsTopology -FileName $cfgfilename

    Set-CsSimpleUrlConfiguration -Instance $simpleUrlObj

    echo ("Publishing Topology...")
    Enable-CsTopology
    echo ("Enabling Lync Server...")
    Enable-CSComputer
    echo ("Update Finished!")    
}
catch{ echo ("Failed of executing script!")}
