﻿#region Apache License
//
// Licensed to the Apache Software Foundation (ASF) under one or more 
// contributor license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright ownership. 
// The ASF licenses this file to you under the Apache License, Version 2.0
// (the "License"); you may not use this file except in compliance with 
// the License. You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace LyncEdgeDynaDNSUpdater
{
    public partial class MainForm : Form
    {
        private String AppFile = String.Empty;
        private String AppPath = String.Empty;
        private String AppName = String.Empty;
        private String CfgName = String.Empty;

        private String DynaDNS = String.Empty;
        private String Edge = String.Empty;
        private Boolean AutoUpdate = true;

        private Int32 timeInterval = 5000;
        private Int32 timeRemaining = 0;
        private IPAddress lastIP = new IPAddress(0);

        public MainForm()
        {
            InitializeComponent();

            this.Icon = Icon.ExtractAssociatedIcon( Application.ExecutablePath );
            //AppTray.Icon = this.Icon;

            AppFile = Process.GetCurrentProcess().MainModule.FileName;
            AppPath = Path.GetDirectoryName( AppFile );
            AppName = Path.GetFileNameWithoutExtension(this.GetType().Module.ToString());
            CfgName = String.Format("{0}\\{1}.config", AppPath, AppName );
        }

        private void MainForm_Load( object sender, EventArgs e )
        {
            LoadFromConfig();
        }

        private void MainForm_FormClosed( object sender, FormClosedEventArgs e )
        {
            SaveToConfig();
        }

        private void AppTimer_Tick( object sender, EventArgs e )
        {
            if ( timeRemaining <= 0 )
            {
                timeRemaining = Convert.ToInt32( edTimerInterval.Value ) * 60 * 1000;
                if ( AutoUpdate ) CheckIP();
            }
            else
            {
                timeRemaining -= this.AppTimer.Interval;
            }
        }

        private void btnUpdate_Click( object sender, EventArgs e )
        {
            timeRemaining = Convert.ToInt32( edTimerInterval.Value ) * 1000;
            CheckIP(true);
        }

        private void cbAutoUpdate_CheckedChanged( object sender, EventArgs e )
        {
            AutoUpdate = this.cbAutoUpdate.Checked;
            //this.AppTimer.Enabled = this.cbAutoUpdate.Checked;
        }

        private void CheckIP( Boolean manual = false )
        {
            DynaDNS = this.edDynaDNS.Text;
            Edge = this.edEdge.Text;
            AutoUpdate = this.cbAutoUpdate.Checked;

            if ( (!String.IsNullOrEmpty( DynaDNS )) && (!String.IsNullOrEmpty( Edge )) )
            {
                try
                {
                    IPHostEntry newHost = Dns.GetHostEntry( DynaDNS );

                    foreach ( IPAddress address in newHost.AddressList )
                    {
                        if ( address.AddressFamily == AddressFamily.InterNetwork )
                        {
                            //if ( !address.Equals( lastIP ) || manual )
                            if ( !address.ToString().Equals( lastIP.ToString() ) || manual )
                            {
                                String AppCmd  = String.Format( "{0}\\{1}.ps1", AppPath, AppName );
                                IDictionary<String, String> AppArgs = new Dictionary<String, String>();
                                AppArgs.Add( "DynaDNS", DynaDNS );
                                AppArgs.Add( "EDGE", Edge );
                                AppArgs.Add( "Topology", "MyTopology.xml" );

                                String args = String.Format( "{0} {1} {2}", DynaDNS, Edge, "MyTopology.xml" );
                                RunScript( File.ReadAllText( AppCmd ), args );
                                //edLog.Clear();
                                //edLog.AppendText( RunScript( File.ReadAllText( AppCmd ), AppArgs ) );
                                lastIP = address;
                            }
                        }
                    }
                }
                catch ( Exception ) { }
                //catch ( Exception ex ) { edLog.AppendText( ex.ToString() ); }
            }
        }

        private void RunScript( String scriptText, String cmdLineArgs )
        {                      
            // create Powershell runspace
            Runspace runspace = RunspaceFactory.CreateRunspace();

            // open it
            runspace.Open();

            // Use the PowerShell.Create and Powers.AddCommand 
            // methods to create a command pipeline that includes 
            // Get-Process cmdlet. Do not include spaces immediatly 
            // before or after the cmdlet name as that will cause 
            // the command to fail.
            PowerShell ps = PowerShell.Create();
            ps.Runspace = runspace;
            ps.AddScript( scriptText );
            //ps.AddScript( scriptText ).AddArgument(Params);
            foreach ( String param in cmdLineArgs.Split( ' ' ) )
            {
                ps.AddArgument( param );
            }

            // Create an IAsyncResult object and call the
            // BeginInvoke method to start running the 
            // pipeline asynchronously.
            IAsyncResult async = ps.BeginInvoke();

            //AsyncCallback 

            // Using the PowerShell.EndInvoke method, get the
            // results from the IAsyncResult object.
            StringBuilder log = null;
            log = new StringBuilder();
            log.AppendLine( String.Format( "==== {0:yyyy-MM-dd HH:mm:ss zzz} ====", DateTime.Now ) );
            edLog.AppendText( log.ToString() );
            foreach ( PSObject result in ps.EndInvoke( async ) )
            {
                log = new StringBuilder();
                log.AppendLine( String.Format( "{0}", result.ToString() ) );
                edLog.AppendText( log.ToString() );
            } // End foreach.
            log = new StringBuilder();
            log.AppendLine( String.Format( "====================================" ) );
            edLog.AppendText( log.ToString() );

            ps.Stop();
            ps.Runspace = null;
            ps.Dispose();
            ps = null;

            runspace.Close();
            runspace.Dispose();
            runspace = null;

            GC.Collect();
        }

        /// <summary>
        /// Runs the given powershell script and returns the script output.
        /// </summary>
        /// <param name="scriptText">the powershell script text to run</param>
        /// <returns>output of the script</returns>
        private String RunScript( String scriptText, IDictionary<String, String> Params)
        {
            // create Powershell runspace
            Runspace runspace = RunspaceFactory.CreateRunspace();

            // open it
            runspace.Open();

            // create a pipeline and feed it the script text
            Pipeline pipeline = runspace.CreatePipeline();

            Command BypassPolicyCmd = new Command( "Set-ExecutionPolicy ByPass", true, false );
            Command RemoteSignedPolicyCmd = new Command( "Set-ExecutionPolicy RemoteSigned", true, false );

            //pipeline.Commands.AddScript( scriptText, false );
            //Command Cmd =   pipeline.Commands[0];

            //pipeline.Commands.Add( BypassPolicyCmd );
            Command Cmd =   new Command( scriptText, true, false );
            #region // Get/Set Params

            String ValueStr = String.Empty;
            CommandParameter cmdParam = null;
            String DynaDNS = String.Empty;
            String Edge = String.Empty;
            String Topology = String.Empty;

            if ( Params.ContainsKey( "DynaDNS" ) && Params.TryGetValue( "DynaDNS", out ValueStr ) )
            {
                DynaDNS = ValueStr;
            }
            else return ( String.Empty );

            if ( Params.ContainsKey( "EDGE" ) && Params.TryGetValue( "EDGE", out ValueStr ) )
            {
                Edge = ValueStr;
            }
            else return ( String.Empty );

            if ( Params.ContainsKey( "Topology" ) && Params.TryGetValue( "Topology", out ValueStr ) )
            {
                Topology = ValueStr;
            }
            cmdParam = new CommandParameter( DynaDNS, Edge );
            Cmd.Parameters.Add( cmdParam );

            cmdParam = new CommandParameter( Topology, "" );
            Cmd.Parameters.Add( cmdParam );

            #endregion Get/Set Params
            pipeline.Commands.Add( Cmd );
            //pipeline.Commands.Add( RemoteSignedPolicyCmd );

            // add an extra command to transform the script output objects into nicely formatted strings
            // remove this line to get the actual objects that the script returns. For example, the script
            // "Get-Process" returns a collection of System.Diagnostics.Process instances.
            //pipeline.Commands.Add( "Out-String" );

            try
            {
                // execute the script
                Collection<PSObject> results = pipeline.Invoke();

                // close the runspace
                runspace.Close();

                // convert the script result into a single string
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.AppendLine( String.Format( "==== {0:yyyy-MM-dd HH:mm:ss zzz} ====", DateTime.Now ) );
                foreach ( PSObject obj in results )
                {
                    stringBuilder.AppendLine( obj.ToString() );
                }
                stringBuilder.AppendLine( String.Format( "====================================") );

                return stringBuilder.ToString();
            }
            catch ( Exception ex ) { return ( ex.ToString() ); }
        }

        private void LoadFromConfig()
        {
            if ( !File.Exists( CfgName ) ) return;

            XmlDocument xmldoc = new XmlDocument();
            xmldoc.Load( CfgName );
            XmlNodeList elements = null;

            #region // Get AutoUpdate Setting
            elements = xmldoc.GetElementsByTagName( "AutoUpdate" );
            foreach ( XmlNode element in elements )
            {
                XmlAttributeCollection attrs = element.Attributes;
                foreach ( XmlAttribute attr in attrs )
                {
                    if ( attr.Name.Equals( "value", StringComparison.InvariantCultureIgnoreCase ) )
                    {
                        Boolean.TryParse( attr.Value, out AutoUpdate );
                        cbAutoUpdate.Checked = AutoUpdate;
                        break;
                    }
                }
            }
            #endregion

            elements = xmldoc.GetElementsByTagName( "DynaDNS_FQDN" );
            foreach ( XmlNode element in elements )
            {
                XmlAttributeCollection attrs = element.Attributes;
                foreach ( XmlAttribute attr in attrs )
                {
                    if ( attr.Name.Equals( "value", StringComparison.InvariantCultureIgnoreCase ) )
                    {
                        DynaDNS = attr.Value;
                        this.edDynaDNS.Text = attr.Value;
                        break;
                    }
                }
            }

            elements = xmldoc.GetElementsByTagName( "Edge_FQDN" );
            foreach ( XmlNode element in elements )
            {
                XmlAttributeCollection attrs = element.Attributes;
                foreach ( XmlAttribute attr in attrs )
                {
                    if ( attr.Name.Equals( "value", StringComparison.InvariantCultureIgnoreCase ) )
                    {
                        Edge = attr.Value;
                        this.edEdge.Text = attr.Value;
                        break;
                    }
                }
            }

            elements = xmldoc.GetElementsByTagName( "CheckInterval" );
            foreach ( XmlNode element in elements )
            {
                XmlAttributeCollection attrs = element.Attributes;
                foreach ( XmlAttribute attr in attrs )
                {
                    if ( attr.Name.Equals( "value", StringComparison.InvariantCultureIgnoreCase ) )
                    {
                        Decimal value = new Decimal(5);
                        Decimal.TryParse( attr.Value, out value );
                        this.edTimerInterval.Value = value;
                        timeInterval = Convert.ToInt32( value );
                        break;
                    }
                }
            }

            elements = xmldoc.GetElementsByTagName( "LastIP" );
            foreach ( XmlNode element in elements )
            {
                XmlAttributeCollection attrs = element.Attributes;
                foreach ( XmlAttribute attr in attrs )
                {
                    if ( attr.Name.Equals( "value", StringComparison.InvariantCultureIgnoreCase ) )
                    {
                        try
                        {
                            IPAddress.TryParse( attr.Value, out lastIP );
                        }
                        catch ( Exception ) { }
                        break;
                    }
                }
            }

        }

        private void SaveToConfig()
        {
            String DynaDNS = this.edDynaDNS.Text;
            String Edge = this.edEdge.Text;
            String Interval = this.edTimerInterval.Value.ToString();

            StringBuilder xml = new StringBuilder();
            xml.AppendLine("<?xml version=\"1.0\"?>");
            xml.AppendLine( "<AppSettings>" );
            xml.AppendLine( "</AppSettings>" );

            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml( xml.ToString() );

            XmlElement root = xmldoc.DocumentElement;

            XmlNode node = null;
            XmlAttributeCollection attrs = null;
            XmlAttribute attr = null;

            node = xmldoc.CreateNode( "element", "AutoUpdate", "" );
            attr = xmldoc.CreateAttribute( "value" );
            attr.Value = this.cbAutoUpdate.Checked.ToString();
            attrs = node.Attributes;
            attrs.Append( attr );
            root.AppendChild( node );

            node = xmldoc.CreateNode( "element", "CheckInterval", "" );
            attr = xmldoc.CreateAttribute( "value" );
            attr.Value = this.edTimerInterval.Value.ToString();
            attrs = node.Attributes;
            attrs.Append( attr );               
            root.AppendChild( node );

            node = xmldoc.CreateNode( "element", "LastIP", "" );
            attr = xmldoc.CreateAttribute( "value" );
            attr.Value = lastIP.ToString();
            attrs = node.Attributes;
            attrs.Append( attr );
            root.AppendChild( node );

            if ( !String.IsNullOrEmpty( DynaDNS ) )
            {
                node = xmldoc.CreateNode( "element", "DynaDNS_FQDN", "" );
                attr = xmldoc.CreateAttribute( "value" );
                attr.Value = DynaDNS;
                attrs = node.Attributes;
                attrs.Append( attr );
                root.AppendChild( node );
            }

            if ( !String.IsNullOrEmpty( Edge ) )
            {
                node = xmldoc.CreateNode( "element", "Edge_FQDN", "" );
                attr = xmldoc.CreateAttribute( "value" );
                attr.Value = Edge;
                attrs = node.Attributes;
                attrs.Append( attr );
                root.AppendChild( node );
            }

            xmldoc.Save( CfgName );
        }


    }

}
